

export class TicTacToe {
    private state:String[][];
    private turn:boolean;

    constructor() {
        this.state = [
            [null, null, null],
            [null, null, null],
            [null, null, null]
        ];
        this.turn = true;
    }

    grid() {

        return this.state;

    }


    play(x: number, y: number) {
        if (this.turn){
            this.state[x][y] = 'X';
        }else{
            this.state[x][y] = 'O';
        }

        this.turn = !this.turn;
    }
}